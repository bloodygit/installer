#!/bin/bash


#todo: Postgress:get shell input and also write it into config.py
#RASPBERRYPI SUPPORT! not just raspian

#bash find OS
if [[ "$OSTYPE" == "linux-gnu"* ]]; then


LD=$(cat /etc/os-release | grep -w "NAME" | sed 's/NAME=//g' | sed 's/"//g')

if [[ $LD == "Arch Linux" ]] || [[ $LD == "Manjaro"* ]]; then
  echo "No ARCH/Manjaro supported yet"
elif [[ $LD == "CentOS"* ]]; then
  echo "No CentOS supported yet"
elif [[ $LD == "Debian"* ]]; then
  echo "No Debian supported yet" #to test, runs also on apt 
elif [[ $LD == "Fedora"* ]]; then
  echo "No Fedora supported yet"
elif [[ $LD == "Mageia"* ]]; then
  echo "No Mageia supported yet"
elif [[ $LD == "openSUSE"* ]]; then
  echo "No openSUSE supported yet"
elif [[ $LD == "Sabayon"* ]]; then
  echo "No Sabayon supported yet"

	# you get the concept...

	elif [[ $LD == "Ubuntu" ]] || [[ $LD == "Zorin OS" ]]; then

	echo "installing requirements via APT for Ubuntu"
	start=`date +%s`

	echo "updaing APT..."
	sudo apt-get update
	sudo apt-get -y upgrade

	# python 3.8
	echo "installing Python 3.8 ..."
	sudo apt-get -y install gcc binutils
	sudo apt-get update
	sudo apt-get -y install software-properties-common
	sudo add-apt-repository -y ppa:deadsnakes/ppa
	sudo apt-get update
	sudo apt-get -y install python-pip python-dev
	sudo apt-get -y install python-setuptools
	sudo apt-get -y install build-essential  python3.8-dev python3-pip
	sudo apt-get update
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 9
	sudo update-alternatives  --set python /usr/bin/python3.8
	echo "Python 3.8 has been set as default python"
	python -m pip install --upgrade pip
	alias pip="pip3" #hash -d pip was illegal in my case
	echo "pip has been set alias for pip3"
	pip install -U setuptools
	pip install ez_setup
	pip install Cython numpy


	# talib
	echo "installing talib ..."
	sudo apt-get -y install libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev python3.8-dev
	wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz -q
	tar -xzf ta-lib-0.4.0-src.tar.gz
	cd ta-lib/
	./configure --prefix=/usr
	make
	sudo make install
	cd ..

	# install PosgreSQL database
	echo "installing PostgreSQL ..."
	sudo apt-get -y install -ywget ca-certificates
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && 	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
	sudo apt-get update
	sudo apt-get -y install postgresql postgresql-contrib python3-psycopg2 libpq-dev

	#PostgresSQL CLI
	#psql
	# create database
	#CREATE DATABASE jesse_db;
	# create new user
	#CREATE USER jesse_user WITH PASSWORD 'password';
	# set privileges of the created user
	#GRANT ALL PRIVILEGES ON DATABASE jesse_db to jesse_user;
	# exit PostgreSQL CLI
	#\q

	# install pip packages
	pip install -r https://raw.githubusercontent.com/jesse-ai/jesse/master/requirements.txt
	pip install jesse

	echo "cleaning up"
	rm ta-lib-0.4.0-src.tar.gz && rm -rf ta-lib
	echo "Finished installation. "
	end=`date +%s`
	runtime=$((end-start))
	echo "Installation took ${runtime} seconds."
	echo "Notice not to use python3 and pip3, but instead use python and pip."
	echo "Here's the output of 'python --version' (it should be 'Python 3.8.*'):"
	python --version
	echo "Here's the output of 'pip --version':"
	pip --version

	read -p "Do you want to install Zsh? <y/N> " prompt
	if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" || $prompt == "YES" ]]
	  then
		sudo apt-get install -y zsh && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	  else
		exit 0
	fi


	read -p "Do you want to install screen? <y/N> " prompt
        if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" || $prompt == "YES" ]]
          then
		sudo apt-get install -y screen

		echo "create and attach to a new screen window"
		echo "screen -S name_of_the_window"
		echo "list all open screens"
		echo "screen -ls"
		echo "reattach to a previously opened window"
		echo "screen -r name_of_the_window"

	else
                exit 0
        fi

	else

	echo "Distro not in install.sh yet, please consider contributing"

	fi


elif [[ "$OSTYPE" == "darwin"* ]]; then
        # Mac OSX


  if [ ! -x "$(command -v brew)" ]
	then
        echo "-------------------------"
        echo "Installing Brew"
        echo "-------------------------"
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi

	brew install python@3.8
	brew install ta-lib
	brew install postgresql

	#PostgresSQL CLI
	#psql
	# create database
	#CREATE DATABASE jesse_db;
	# create new user
	#CREATE USER jesse_user WITH PASSWORD 'password';
	# set privileges of the created user
	#GRANT ALL PRIVILEGES ON DATABASE jesse_db to jesse_user;
	# exit PostgreSQL CLI
	#\q

	pip install -r https://raw.githubusercontent.com/jesse-ai/jesse/master/requirements.txt
	pip install jesse

elif [[ "$OSTYPE" == "cygwin" ]]; then
	echo "Please inspect install.sh and consider contributing"
        # POSIX compatibility layer and Linux environment emulation for Windows
elif [[ "$OSTYPE" == "msys" ]]; then
	echo "Please inspect install.sh and consider contributing"
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
elif [[ "$OSTYPE" == "win32" ]]; then
	echo "Please follow https://github.com/jesse-ai/docs"
elif [[ "$OSTYPE" == "win64" ]]; then
	echo "Please follow https://github.com/jesse-ai/docs"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
	echo "Please inspect install.sh and consider contributing"
else
        # Unknown.
	echo "Unknown OS! Please follow https://github.com/jesse-ai/docs and consider contributing"
fi
